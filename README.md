# StakkKit

[![CI Status](http://img.shields.io/travis/Derek/StakkKit.svg?style=flat)](https://travis-ci.org/Derek/StakkKit)
[![Version](https://img.shields.io/cocoapods/v/StakkKit.svg?style=flat)](http://cocoapods.org/pods/StakkKit)
[![License](https://img.shields.io/cocoapods/l/StakkKit.svg?style=flat)](http://cocoapods.org/pods/StakkKit)
[![Platform](https://img.shields.io/cocoapods/p/StakkKit.svg?style=flat)](http://cocoapods.org/pods/StakkKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

StakkKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "StakkKit"
```

After installing the pod, please add `StakkKit.xcdatamodeld` into your project, you may rename `StakkKit.xcdatamodeld` to any name you like:
![Alt text](./img/step_1.png)
![Alt text](./img/step_2.png)

Setup StakkKit within `- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;`

```objective-c
+ (void)setupStakkKitWithLoggingLevel:(SFLogLevel)logLevel databaseStoreName:(NSString *)storeName;
```

## Author

Derek, derek@stakkfactory.com

## License

StakkKit is available under the MIT license. See the LICENSE file for more info.
