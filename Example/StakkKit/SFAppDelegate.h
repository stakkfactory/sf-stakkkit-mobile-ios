//
//  SFAppDelegate.h
//  StakkKit
//
//  Created by Derek on 09/27/2016.
//  Copyright (c) 2016 Derek. All rights reserved.
//

#import <StakkKit/SFPushEnabledAppDelegate.h>

@interface SFAppDelegate : SFPushEnabledAppDelegate <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
