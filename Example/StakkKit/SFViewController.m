//
//  SFViewController.m
//  StakkKit
//
//  Created by Derek on 09/27/2016.
//  Copyright (c) 2016 Derek. All rights reserved.
//

#import "SFViewController.h"

// StakkKit
#import "StakkKit.h"

@interface SFViewController () <SFNetworkManagerDelegate>

@end

@implementation SFViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self setupStakkKit];
    [self makeSampleRequest];
    [self makeFailRequest];
    [self dateStringConvertExample];
	[self monitorNetworkStatus];
}

#pragma mark - Helpers

- (void)setupStakkKit {
    
    [StakkKit setupStakkKitWithLoggingLevel:SFLogLevelAll databaseStoreName:@"StakkKitDatabase"];
    
    SFLogError(@"Error Log");
    SFLogDB(@"DB Log");
    SFLogAPI(@"API Log");
    SFLogDebug(@"Debug Log");
}

- (void)makeSampleRequest {
    
    SFNetworkManager *manager = [SFNetworkManager sharedInstance];
    
    [manager requestWithURL:@"https://mxlbw0t3ia.execute-api.ap-northeast-1.amazonaws.com/prod/startup"
                     method:SFRequestMethodGET
                 parameters:nil
                ignoreCache:NO
          cachePeriodInSecs:6000
                    success:^(NSDictionary *responseDict) {
                        
                        SFLogDebug(@"Request succeed");
                    }
                    failure:^(NSError *error, NSDictionary *responseDict) {
                        
                        SFLogError(@"Request failed");
                    }];
}

- (void)makeFailRequest {
    
    SFNetworkManager *manager = [SFNetworkManager sharedInstance];
    
    [manager requestWithURL:@"https://api.stakk.com/mate-dev/call/request"
                     method:SFRequestMethodGET
                 parameters:nil
                ignoreCache:NO
          cachePeriodInSecs:6000
                    success:nil
                    failure:nil];
}

- (void)dateStringConvertExample {
    
    NSDate *nowDate = [NSDate date];
    
    NSString *nowString = [nowDate iso8601String];
    SFLogDebug(@"nowString = %@", nowString);
    
    NSDate *dateFromString = [nowString iso8601Date];
    SFLogDebug(@"dateFromString = %@", dateFromString);
}

- (void)monitorNetworkStatus {

	[[SFNetworkManager sharedInstance] addDelegate:self];

	[SFNetworkManager sharedInstance].enableNetworkStatusTracking = YES;
}

#pragma mark - <SFNetworkManagerDelegate>

- (void)managerNetworkStatusDidChange:(SFNetworkManager *)manager {

	SFNetworkStatus currentStatus = manager.networkStatus;

	SFLogDebug(@"currentStatus = %@", [SFNetworkManager networkStatusName:currentStatus]);
}

@end
