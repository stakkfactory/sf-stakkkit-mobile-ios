//
//  SFDatabaseManager.h
//  Pods
//
//  Created by Derek on 28/9/2016.
//
//

#import "SFBaseManager.h"

// Models
#import "SFDBAPICache.h"

typedef void (^SFDatabaseCompletionBlock)(BOOL success);

@interface SFDatabaseManager : SFBaseManager

- (void)setupWithStoreName:(NSString *)storeName;

- (void)reset;

- (void)createOrUpdateAPICacheWithURL:(NSString *)url
                               method:(NSString *)method
                           parameters:(NSDictionary *)parameters
                             response:(NSDictionary *)response
                    cachePeriodInSecs:(CGFloat)cachePeriodInSecs
                             complete:(SFDatabaseCompletionBlock)complete;

- (SFDBAPICache *)getAPICacheWithURL:(NSString *)url
                              method:(NSString *)method
                          parameters:(NSDictionary *)parameters;

@end
