//
//  SFBaseManager.m
//  Pods
//
//  Created by Derek on 13/6/2017.
//
//

#import "SFBaseManager.h"

@implementation SFBaseManager

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    
    static dispatch_once_t once;
    static NSMutableDictionary *sharedInstances;
    
    dispatch_once(&once, ^{ /* This code fires only once */
        
        // Creating of the container for shared instances for different classes
        sharedInstances = [NSMutableDictionary new];
    });
    
    id sharedInstance;
    
    @synchronized(self) { /* Critical section for Singleton-behavior */
        
        // Getting of the shared instance for exact class
        sharedInstance = sharedInstances[NSStringFromClass(self)];
        
        if (!sharedInstance) {
            
            // Creating of the shared instance if it's not created yet
            sharedInstance = [self new];
            sharedInstances[NSStringFromClass(self)] = sharedInstance;
            [sharedInstance setup];
        }
    }
    
    return sharedInstance;
}

#pragma mark - Setup

- (void)setup {
    
    // Override hook
}

@end
