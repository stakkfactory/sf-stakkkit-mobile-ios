//
//  SFMultiDelegateManager+Private.h
//  Pods
//
//  Created by Derek Cheung on 20/6/2017.
//
//

#import "SFMultiDelegateManager.h"

@interface SFMultiDelegateManager ()

- (void)notifyDelegatesWithMethod:(SEL)selector;

@end

