//
//  SFBaseManager.h
//  Pods
//
//  Created by Derek on 13/6/2017.
//
//

#import <Foundation/Foundation.h>

@interface SFBaseManager : NSObject

+ (instancetype)sharedInstance;

// Override hook
- (void)setup;

@end
