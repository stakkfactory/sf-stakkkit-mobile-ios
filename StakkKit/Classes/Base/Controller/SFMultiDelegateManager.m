//
//  SFMultiDelegateManager.m
//  Pods
//
//  Created by Derek on 13/6/2017.
//
//

#import "SFMultiDelegateManager.h"

@interface SFMultiDelegateManager ()

@property (nonatomic, strong) NSMutableArray *delegates;

@end

@implementation SFMultiDelegateManager

#pragma mark - Override

- (void)setup {
    
    [super setup];
    
    _delegates = [NSMutableArray new];
}

#pragma mark - Public

- (void)addDelegate:(id)delegate {
    
    BOOL isDelegateExist = ([self.delegates indexOfObject:delegate] != NSNotFound);
    
    if (!isDelegateExist) {
        
        [self.delegates addObject:delegate];
    }
}

- (void)removeDelegate:(id)delegate {
    
    BOOL isDelegateExist = ([self.delegates indexOfObject:delegate] != NSNotFound);
    
    if (isDelegateExist) {
        
        [self.delegates removeObject:delegate];
    }
}

- (void)removeAllDelegates {
    
    [self.delegates removeAllObjects];
}

#pragma mark - Private

- (void)notifyDelegatesWithMethod:(SEL)selector {
    
    // This trick is for prevent the self.delegates is being modified at the same time
    NSArray *tempArray = [NSArray arrayWithArray:self.delegates];
    
    for (id delegate in tempArray) {
        
        if ([delegate respondsToSelector:selector]) {
            
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [delegate performSelector:selector withObject:self];
#pragma clang diagnostic pop
        }
    }
}

@end
