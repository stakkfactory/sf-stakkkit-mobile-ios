//
//  SFMultiDelegateManager.h
//  Pods
//
//  Created by Derek on 13/6/2017.
//
//

#import "SFBaseManager.h"

@interface SFMultiDelegateManager : SFBaseManager

- (void)addDelegate:(id)delegate;
- (void)removeDelegate:(id)delegate;
- (void)removeAllDelegates;

@end
