//
//  SFBaseView.h
//  Pods
//
//  Created by Derek on 29/9/2016.
//
//

#import <UIKit/UIKit.h>

@interface SFBaseView : UIView

- (void)setupAll; // Combined call for setup, setupLayer and setupConstraints

- (void)setup; // Override hook

- (void)setupLayer; // Override hook

- (void)setupConstraints; // Override hook

@end
