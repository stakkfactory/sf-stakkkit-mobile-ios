//
//  StakkKit.m
//  Pods
//
//  Created by Derek on 6/1/2017.
//
//

#import "StakkKit.h"

@implementation StakkKit

+ (void)setupStakkKitWithLoggingLevel:(SFLogLevel)logLevel databaseStoreName:(NSString *)storeName {
    
    [SFLoggingManager setupWithLevel:logLevel];
    
    [[SFDatabaseManager sharedInstance] setupWithStoreName:storeName];
}

@end
