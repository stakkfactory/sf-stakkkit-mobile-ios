//
//  SFNetworkManager.m
//  Pods
//
//  Created by Derek on 27/9/2016.
//
//

#import "SFNetworkManager.h"

// Frameworks
#import <AFNetworking/AFNetworking.h>
#import <libextobjc/EXTScope.h>

//Models
#import "SFLog.h"

// Controllers
#import "SFDatabaseManager.h"

// Privates
#import "SFMultiDelegateManager+Private.h"

// Constants
static NSString * const kDefaultResponseKey = @"response";

typedef void (^SFActionSuccessBlock)(NSURLSessionDataTask *task, NSString *url, NSDictionary *responseDict, SFRequestSuccessBlock successBlock, BOOL isFromCache, CGFloat cachePeriodInSecs);
typedef void (^SFActionFailureBlock)(NSURLSessionDataTask *task, NSString *url, NSError *error, SFRequestFailureBlock failure);

@interface SFNetworkManager ()

@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;
@property (nonatomic, assign) SFNetworkStatus networkStatus;

@end

@implementation SFNetworkManager

#pragma mark - Override

- (void)setup {
    
    [super setup];
    
    self.sessionManager = [AFHTTPSessionManager manager];
	self.networkStatus = SFNetworkStatusUndetermine;
}

- (void)addDelegate:(id<SFNetworkManagerDelegate>)delegate {

	[super addDelegate:delegate];
}

- (void)removeDelegate:(id<SFNetworkManagerDelegate>)delegate {

	[super removeDelegate:delegate];
}

#pragma mark - Public

- (NSURLSessionDataTask *)requestWithURL:(NSString *)url
                                  method:(SFRequestMethod)method
                              parameters:(NSDictionary *)parameters
                             ignoreCache:(BOOL)ignoreCache
                       cachePeriodInSecs:(CGFloat)cachePeriodInSecs
                                 success:(SFRequestSuccessBlock)success
                                 failure:(SFRequestFailureBlock)failure {
    
    // Success action block
    SFActionSuccessBlock successActionBlock = ^(NSURLSessionDataTask *task,
                                                NSString *url,
                                                id responseObject,
                                                SFRequestSuccessBlock successBlock,
                                                BOOL isFromCache,
                                                CGFloat cachePeriodInSecs) {
        
        NSInteger httpStatusCode = [SFNetworkManager httpStatusCodeFromTask:task];

		NSDictionary *responseDict = [SFNetworkManager dictionaryWithResponseObject:responseObject];
        
        [SFNetworkManager endRequestLoggingWithURL:url
                                       isFromCache:isFromCache
                                         isSuccess:YES
                                    responseObject:responseDict
                                             error:nil
                                    httpStatusCode:httpStatusCode];
        
        if (!isFromCache && cachePeriodInSecs > 0) {
            
            [[SFDatabaseManager sharedInstance] createOrUpdateAPICacheWithURL:url
                                                                       method:[SFNetworkManager stringForMethod:method]
                                                                   parameters:parameters
                                                                     response:responseDict
                                                            cachePeriodInSecs:cachePeriodInSecs
                                                                     complete:^(BOOL success) {
                                                                         
                                                                         if (successBlock) {
                                                                             
                                                                             successBlock(responseDict);
                                                                         }
                                                                     }];
        } else {
            
            if (successBlock) {
                
                successBlock(responseDict);
            }
        }
    };
    
    // Failure action block
    SFActionFailureBlock failureActionBlock = ^(NSURLSessionDataTask *task,
                                                NSString *url,
                                                NSError *error,
                                                SFRequestFailureBlock failure) {
        
        NSDictionary *responseDict = [SFNetworkManager responseDictFromError:error];
        NSInteger httpStatusCode = [SFNetworkManager httpStatusCodeFromTask:task];
        
        [SFNetworkManager endRequestLoggingWithURL:url
                                       isFromCache:NO
                                         isSuccess:NO
                                    responseObject:responseDict
                                             error:error
                                    httpStatusCode:httpStatusCode];
        
        if (failure) {
            
            failure(error, responseDict);
        }
    };
    
    // Start request logging
    [SFNetworkManager startRequestLoggingWithMethod:method URL:url parameters:parameters];
    
    NSURLSessionDataTask *task = nil;
    
    SFDBAPICache *model = nil;
    
    if (!ignoreCache) {
        // Try to get response from cache
        model = [[SFDatabaseManager sharedInstance] getAPICacheWithURL:url
                                                                method:[SFNetworkManager stringForMethod:method]
                                                            parameters:parameters];
    }
    
    if (model) {
        
        successActionBlock(nil, url, model.response, success, YES, cachePeriodInSecs);
        
    } else {
        
        AFHTTPSessionManager *manager = self.sessionManager;
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        if (method == SFRequestMethodGET) {
            
            task = [manager GET:url
                     parameters:parameters
                       progress:nil
                        success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                            
                            successActionBlock(task, url, responseObject, success, NO, cachePeriodInSecs);
                            
                        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                            
                            failureActionBlock(task, url, error, failure);
                        }];
            
        } else if (method == SFRequestMethodPOST) {
            
            task = [manager POST:url
                      parameters:parameters
                        progress:nil
                         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                             
                             successActionBlock(task, url, responseObject, success, NO, cachePeriodInSecs);
                             
                         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                             
                             failureActionBlock(task, url, error, failure);
                         }];

        } else if (method == SFRequestMethodPATCH) {

            task = [manager PATCH:url
                       parameters:parameters
                          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

                              successActionBlock(task, url, responseObject, success, NO, cachePeriodInSecs);

                          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

                              failureActionBlock(task, url, error, failure);
                          }];
        } else {

            task = [manager PUT:url
                     parameters:parameters
                        success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

                            successActionBlock(task, url, responseObject, success, NO, cachePeriodInSecs);

                        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

                            failureActionBlock(task, url, error, failure);
                        }];
        }
    }

    return task;
}

+ (NSString *)networkStatusName:(SFNetworkStatus)status {

	NSString *name = @"";

	switch (status) {

		case SFNetworkStatusUndetermine:
			name = @"SFNetworkStatusUndetermine";
			break;

		case SFNetworkStatusNotReachable:
			name = @"SFNetworkStatusNotReachable";
			break;

		case SFNetworkStatusReachableViaWifi:
			name = @"SFNetworkStatusReachableViaWifi";
			break;

		case SFNetworkStatusReachableViaWWAN:
			name = @"SFNetworkStatusReachableViaWWAN";
			break;
	}

	return name;
}

+ (NSString *)defaultResponseKey {

	return kDefaultResponseKey;
}

#pragma mark - Helpers

+ (void)startRequestLoggingWithMethod:(SFRequestMethod)method
                                  URL:(NSString *)url
                           parameters:(NSDictionary *)parameters {
    
    NSMutableString *logString = [[NSMutableString alloc] init];
    
    [logString appendString:@"---Request started---\n"];
    [logString appendFormat:@"URL: %@\n", url];
    [logString appendFormat:@"Method: %@\n", [SFNetworkManager stringForMethod:method]];
    [logString appendFormat:@"Parameters: %@\n", parameters];
    [logString appendString:@"---Request started---"];
    
    SFLogAPI(@"%@", logString);
}


+ (void)endRequestLoggingWithURL:(NSString *)url
                     isFromCache:(BOOL)isFromCache
                       isSuccess:(BOOL)success
                  responseObject:(NSDictionary *)responseObject
                           error:(NSError *)error
                  httpStatusCode:(NSInteger)httpStatusCode{
    
    NSMutableString *logString = [[NSMutableString alloc] init];
    
    [logString appendString:@"---Request ended---\n"];
    [logString appendFormat:@"URL: %@\n", url];
    [logString appendFormat:@"Status: %@\n", success ? @"Success" : @"Fail"];
    [logString appendFormat:@"FromCache: %@\n", isFromCache ? @"YES" : @"NO"];
    [logString appendFormat:@"HTTPStatusCode: %d\n", (int)httpStatusCode];
    
    if (!success) {
        
        if (error) {
            
            [logString appendFormat:@"ErrorDescription: %@\n", [error localizedDescription]];
        }
    }
    
    [logString appendFormat:@"ResponseObject: %@\n", responseObject];
    
    [logString appendString:@"---Request ended---"];
    
    SFLogAPI(@"%@", logString);
}

+ (NSString *)stringForMethod:(SFRequestMethod)method {
    
    if (method == SFRequestMethodGET) {
        
        return @"GET";
        
    } else if (method == SFRequestMethodPOST) {
        
        return @"POST";

    } else if (method == SFRequestMethodPATCH) {

        return @"PATCH";

    } else {

        return @"PUT";
    }
}

+ (NSDictionary *)responseDictFromError:(NSError *)error {
    
    NSDictionary *responseDict = nil;
    
    NSData *errorData = [error.userInfo objectForKey:AFNetworkingOperationFailingURLResponseDataErrorKey];
    
    if (errorData) {
        
        responseDict = [NSJSONSerialization JSONObjectWithData:errorData options:kNilOptions error:nil];
    }
    
    return responseDict;
}

+ (NSInteger)httpStatusCodeFromTask:(NSURLSessionDataTask *)task {
    
    NSInteger httpStatusCode = -1;
    
    if ([task.response isKindOfClass:[NSHTTPURLResponse class]]) {
        
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        
        httpStatusCode = response.statusCode;
    }
    
    return httpStatusCode;
}

- (void)startMonitoringNetworkStatus {

	SFLogAPI(@"Start monitoring network status");

	AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];

	@weakify(self)
	[manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {

		@strongify(self)
		[self notifyDelegatesNetworkStatusDidChange:status];
	}];

	[manager startMonitoring];
}

- (void)stopMonitoringNetworkStatus {

	SFLogAPI(@"Stop monitoring network status");

	AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
	[manager stopMonitoring];
}

- (void)notifyDelegatesNetworkStatusDidChange:(AFNetworkReachabilityStatus)status {

	SFNetworkStatus lastStatus = self.networkStatus;
	SFNetworkStatus currentStatus = lastStatus;

	switch (status) {

		case AFNetworkReachabilityStatusNotReachable:
			currentStatus = SFNetworkStatusNotReachable;
			break;

		case AFNetworkReachabilityStatusReachableViaWiFi:
			currentStatus = SFNetworkStatusReachableViaWifi;
			break;

		case AFNetworkReachabilityStatusReachableViaWWAN:
			currentStatus = SFNetworkStatusReachableViaWWAN;
			break;

		default:
			currentStatus = SFNetworkStatusUndetermine;
			break;
	}

	if (lastStatus != currentStatus) {

		NSString *lastStatusString = [SFNetworkManager networkStatusName:lastStatus];
		NSString *currentStatusString = [SFNetworkManager networkStatusName:currentStatus];
		SFLogAPI(@"Network status did change!\n%@ -> %@", lastStatusString, currentStatusString);

		self.networkStatus = currentStatus;
		[self notifyDelegatesWithMethod:@selector(managerNetworkStatusDidChange:)];
	}
}

+ (NSDictionary *)dictionaryWithResponseObject:(id)responseObject {

	NSDictionary *output = nil;

	if (responseObject && ![responseObject isKindOfClass:[NSDictionary class]]) {

		output = [NSDictionary dictionaryWithObject:responseObject forKey:kDefaultResponseKey];

	} else {

		output = responseObject;
	}

	return output;
}

#pragma mark - Setters

- (void)setEnableNetworkStatusTracking:(BOOL)enableNetworkStatusTracking {

	if (_enableNetworkStatusTracking != enableNetworkStatusTracking) {

		_enableNetworkStatusTracking = enableNetworkStatusTracking;

		if (_enableNetworkStatusTracking) {

			[self startMonitoringNetworkStatus];

		} else {

			[self stopMonitoringNetworkStatus];
		}
	}
}

@end
