//
//  SFNetworkManager.h
//  Pods
//
//  Created by Derek on 27/9/2016.
//
//

#import "SFMultiDelegateManager.h"

// Constants
typedef enum {
    SFRequestMethodGET,
    SFRequestMethodPOST,
    SFRequestMethodPATCH,
    SFRequestMethodPUT
} SFRequestMethod;

typedef enum {
	SFNetworkStatusUndetermine,
	SFNetworkStatusNotReachable,
	SFNetworkStatusReachableViaWWAN,
	SFNetworkStatusReachableViaWifi
} SFNetworkStatus;

typedef void (^SFRequestSuccessBlock)(NSDictionary *responseDict);
typedef void (^SFRequestFailureBlock)(NSError *error, NSDictionary *responseDict);

@class SFNetworkManager;

@protocol SFNetworkManagerDelegate <NSObject>

- (void)managerNetworkStatusDidChange:(SFNetworkManager *)manager;

@end

@interface SFNetworkManager : SFMultiDelegateManager

@property (nonatomic, assign) BOOL enableNetworkStatusTracking;
@property (nonatomic, assign, readonly) SFNetworkStatus networkStatus;

- (void)addDelegate:(id<SFNetworkManagerDelegate>)delegate;
- (void)removeDelegate:(id<SFNetworkManagerDelegate>)delegate;

- (NSURLSessionDataTask *)requestWithURL:(NSString *)url
                                  method:(SFRequestMethod)method
                              parameters:(NSDictionary *)parameters
                             ignoreCache:(BOOL)ignoreCache
                       cachePeriodInSecs:(CGFloat)cachePeriodInSecs
                                 success:(SFRequestSuccessBlock)success
                                 failure:(SFRequestFailureBlock)failure;

+ (NSString *)networkStatusName:(SFNetworkStatus)status;

+ (NSString *)defaultResponseKey;

@end
