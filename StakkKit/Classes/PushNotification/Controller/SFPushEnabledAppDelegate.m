//
//  SFPushEnabledAppDelegate.m
//  Pods
//
//  Created by Derek on 12/1/2017.
//
//

#import "SFPushEnabledAppDelegate.h"

// Frameworks
#import <UserNotifications/UserNotifications.h>

// Macro
#import <libextobjc/EXTScope.h>
#define SYSTEM_VERSION_GRATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

// Logging
#import "SFLog.h"

@interface SFPushEnabledAppDelegate () <UNUserNotificationCenterDelegate>

@end

@implementation SFPushEnabledAppDelegate

#pragma mark - Public

- (void)setupUserNotificationCenterDelegate {
    // The delegate must be set before the application returns from applicationDidFinishLaunching:.
    
    if (SYSTEM_VERSION_GRATER_THAN_OR_EQUAL_TO(@"10.0")) {
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        
        center.delegate = self;
    }
}

- (void)registerForRemoteNotifications {
    
    if (SYSTEM_VERSION_GRATER_THAN_OR_EQUAL_TO(@"10.0")) {
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        
        @weakify(self)
        [center requestAuthorizationWithOptions:UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge
                              completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                  
                                  if (granted) {
                                      
                                      [[UIApplication sharedApplication] registerForRemoteNotifications];
                                      
                                  } else {
                                      
                                      @strongify(self)
                                      [self didFailToRegisterForRemoteNotificationsWithError:error];
                                  }
                              }];
        
    } else {
        
        // Code for old versions
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge
                                                                                             categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    }
}

+ (void)getPushAuthorizationWithCompletionHandler:(void(^)(SFPushAuthorizationStatus status))completionHandler {
    
    if (SYSTEM_VERSION_GRATER_THAN_OR_EQUAL_TO(@"10.0")) {
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
            
            if (completionHandler) {
                
                SFPushAuthorizationStatus status = SFPushAuthorizationStatusNotDetermined;
                
                switch (settings.authorizationStatus) {
                        
                    case UNAuthorizationStatusAuthorized:
                        status = SFPushAuthorizationStatusAuthorized;
                        break;
                        
                    case UNAuthorizationStatusDenied:
                        status = SFPushAuthorizationStatusDenied;
                        break;
                        
                    case UNAuthorizationStatusNotDetermined:
                        status = SFPushAuthorizationStatusNotDetermined;
                        break;
                }
                
                completionHandler(status);
            }
        }];
        
    } else {
        
        if (completionHandler) {
            
            UIUserNotificationSettings *settings = [UIApplication sharedApplication].currentUserNotificationSettings;
            
            if (settings) {
                
                if (settings.types == UIUserNotificationTypeNone) {
                    
                    completionHandler(SFPushAuthorizationStatusDenied);
                    
                } else {
                    
                    completionHandler(SFPushAuthorizationStatusAuthorized);
                }
                
            } else {
                
                completionHandler(SFPushAuthorizationStatusNotDetermined);
            }
        }
    }
}

#pragma mark - Push notifications

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    
    if (notificationSettings.types == UIUserNotificationTypeNone) {
        
        [self didFailToRegisterForRemoteNotificationsWithError:nil];
        
    } else {
        
        // Register to receive notifications
        [application registerForRemoteNotifications];
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSString *token = [NSString stringWithFormat:@"%@", deviceToken];
    
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    [self didSuccessToRegisterForRemoteNotificationsWithToken:token];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    [self didFailToRegisterForRemoteNotificationsWithError:error];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void(^)(UIBackgroundFetchResult))completionHandler{
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        
        if (SYSTEM_VERSION_GRATER_THAN_OR_EQUAL_TO(@"10.0")) {
            
            SFLogDebug(@"iOS version >= 10. Let NotificationCenter handle this one." );
            
        } else {
            
            SFLogDebug(@"Handle push from active");
            
            [self didReceiveRemoteNotificationFromActive:userInfo];
        }
        
    } else if ([UIApplication sharedApplication].applicationState == UIApplicationStateInactive) {
        
        if (SYSTEM_VERSION_GRATER_THAN_OR_EQUAL_TO(@"10.0")) {
            
            SFLogDebug(@"iOS version >= 10. Let NotificationCenter handle this one." );
            
        } else {
            
            SFLogDebug(@"Open app with push notifcation.");
            
            [self didOpenAppFromRemoteNotification:userInfo];
        }
        
    } else {
        
        // Only works when enabling background mode with remote-notification
        SFLogDebug(@"Handle push from background");
        
        [self didReceiveRemoteNotificationFromBackground:userInfo];
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    // For iOS >= 10.0
    
    SFLogDebug(@"Handle push from foreground");
    
    [self didReceiveRemoteNotificationFromActive:notification.request.content.userInfo];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler {
    // For iOS >= 10.0
    
    SFLogDebug(@"Open app with push notifcation.");
    
    [self didOpenAppFromRemoteNotification:response.notification.request.content.userInfo];
    
    completionHandler();
}

#pragma mark - Override hooks

- (void)didSuccessToRegisterForRemoteNotificationsWithToken:(NSString *)token {
    // Centralized callback for registering remote notification successfully
    
    SFLogDebug(@"Register push notification succeed.\nToken = %@", token);
    
    // Override hook
}

- (void)didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    // Centralized callback for registering remote notification unsuccessfully
    
    SFLogDebug(@"Register push notification failed.\nError message = %@", error.localizedDescription);
    
    // Override hook
}

- (void)didReceiveRemoteNotificationFromActive:(NSDictionary *)userInfo {
    // Centralized callback for receiving remote notification from active
    
    SFLogDebug(@"Handle push from active.\nUserInfo = %@", userInfo);
    
    // Override hook
}

- (void)didReceiveRemoteNotificationFromBackground:(NSDictionary *)userInfo {
    // Centralized callback for receiving remote notification from background
    // Only works when enabling background mode with remote-notification
    
    SFLogDebug(@"Handle push from background.\nUserInfo = %@", userInfo);
    
    // Override hook
}

- (void)didOpenAppFromRemoteNotification:(NSDictionary *)userInfo {
    // Centralized callback for opening the app with remote notification
    
    SFLogDebug(@"Open app from push notifcation.\nUserInfo = %@", userInfo);
    
    // Override hook
}

@end
