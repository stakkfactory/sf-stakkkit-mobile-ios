//
//  SFPushEnabledAppDelegate.h
//  Pods
//
//  Created by Derek on 12/1/2017.
//
//

#import <UIKit/UIKit.h>

// Constants
typedef enum {
    SFPushAuthorizationStatusNotDetermined = 0,
    SFPushAuthorizationStatusDenied,
    SFPushAuthorizationStatusAuthorized
} SFPushAuthorizationStatus;

@interface SFPushEnabledAppDelegate : UIResponder

// This method must be called before the application returns from applicationDidFinishLaunching:.
- (void)setupUserNotificationCenterDelegate;

// To register for Push notifications
- (void)registerForRemoteNotifications;

// To get the authorization status for Push notifications
+ (void)getPushAuthorizationWithCompletionHandler:(void(^)(SFPushAuthorizationStatus status))completionHandler;

- (void)didSuccessToRegisterForRemoteNotificationsWithToken:(NSString *)token;  // Override hook
- (void)didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;      // Override hook
- (void)didReceiveRemoteNotificationFromActive:(NSDictionary *)userInfo;        // Override hook
- (void)didReceiveRemoteNotificationFromBackground:(NSDictionary *)userInfo;    // Override hook
- (void)didOpenAppFromRemoteNotification:(NSDictionary *)userInfo;              // Override hook

@end
