//
//  SFLoggingManager.m
//  Pods
//
//  Created by Derek on 28/9/2016.
//
//

#import "SFLoggingManager.h"

@implementation SFLoggingManager

+ (void)setupWithLevel:(SFLogLevel)level {
    
    [DDLog addLogger:[DDTTYLogger sharedInstance] withLevel:(DDLogLevel)level];
    [DDLog addLogger:[DDASLLogger sharedInstance] withLevel:(DDLogLevel)level];
}

@end
